#include <perception_common/perception_common.hpp>
#include <boost/thread.hpp>
#include <pcl_ros/point_cloud.h>

void spinner()
{
    while (ros::ok())
        ros::spinOnce();
}

int main(int argc, char *argv[])
{
    ros::init(argc, argv, "test_disparity");
    ros::NodeHandle nh;

    ros::Publisher                  cloud_3d_pub;

    cloud_3d_pub = nh.advertise<sensor_msgs::PointCloud2>("stereo_cloud_computed", 1, true);

    perception_common::SrcVision    src_vision(&nh);

    cv::namedWindow("test_disp_image", cv::WINDOW_AUTOSIZE);

    ros::Rate loop(100);

    pcl::PointCloud<pcl::PointXYZ>  output_cloud;

    while (ros::ok())
    {
        cv::Mat disp_img;
        cv::Mat_<double> q_matrix;
        cv::Mat img_3d;

        src_vision.getDisparityImage(disp_img);
        q_matrix = src_vision.getQMatrix();
        src_vision.get3dImage(disp_img, q_matrix, img_3d);
        output_cloud.resize(disp_img.rows * disp_img.cols);
        output_cloud.height = disp_img.rows;
        output_cloud.width = disp_img.cols;
        for (size_t u = 0; u < disp_img.rows; ++u)
            for (size_t v = 0; v < disp_img.cols; ++v)
            {
                if (disp_img.at<float>(cv::Point(v, u)) == 0.0)
                    continue;

                cv::Vec3f pt_3d;
                pt_3d = src_vision.get3dPoint(img_3d, u, v);

                pcl::PointXYZ pt;
                pt.x = pt_3d.val[0];
                pt.y = pt_3d.val[1];
                pt.z = pt_3d.val[2];
                output_cloud.at(v, u) = pt;
            }

        if ((disp_img.rows > 0) && (disp_img.cols > 0))
        {
            cv::imshow("test_disp_image", disp_img);

            pcl::PCLPointCloud2 output_msg;
            pcl::toPCLPointCloud2(output_cloud, output_msg);
            output_msg.header.frame_id = "/left_camera_optical_frame";
            cloud_3d_pub.publish(output_msg);

            ros::spinOnce();
            cv::waitKey(1);
        }

        ros::spinOnce();

        loop.sleep();
    }
}
