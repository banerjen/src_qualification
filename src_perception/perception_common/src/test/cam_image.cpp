#include <perception_common/perception_common.hpp>
#include <boost/thread.hpp>

void spinner()
{
    while (ros::ok())
        ros::spinOnce();
}

int main(int argc, char *argv[])
{
    ros::init(argc, argv, "test_cam_image");
    ros::NodeHandle nh;

//    boost::thread spinner_thread(spinner);

    perception_common::SrcVision    src_vision(&nh);

    cv::namedWindow("test_cam_image", cv::WINDOW_AUTOSIZE);

    ros::Rate loop(100);

    while (ros::ok())
    {
        cv::Mat img;
        std_msgs::Header header;

        src_vision.getLeftCameraImage(img, header);

        if ((img.rows > 0) && (img.cols > 0))
        {
            cv::imshow("test_cam_image", img);
            ros::spinOnce();
            cv::waitKey(1);
        }

        ros::spinOnce();

        loop.sleep();
    }

//    spinner_thread.join();
}
