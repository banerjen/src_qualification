#include <perception_common/perception_common.hpp>

using namespace perception_common;

SrcVision::SrcVision(ros::NodeHandle *nh):_nh(nh)
{
    _left_camera_img_sub = nh->subscribe(TopicNames::LEFT_CAMERA_IMAGE, 1, &SrcVision::leftCameraCb, this);
    _disparity_img_sub = nh->subscribe(TopicNames::DISPARITY_IMAGE, 1, &SrcVision::disparityCb, this);
    _new_left_camera_image_received = false;
    _new_disparity_image_received = false;

    _q_matrix = cv::Mat_<double>(4, 4, 0.0);
    _q_matrix(0, 0) =  610.1799470098168 * -0.07;
    _q_matrix(1, 1) =  610.1799470098168 * -0.07;
    _q_matrix(0, 3) = -610.1799470098168 * -0.07 * 512.5;
    _q_matrix(1, 3) = -610.1799470098168 * -0.07 * 272.5;
    _q_matrix(2, 3) =  610.1799470098168 * -0.07 * 610.1799470098168;
    _q_matrix(3, 2) = -610.1799470098168;
    _q_matrix(3, 3) =  0.0;
}

SrcVision::~SrcVision()
{
    // Destructor
}

bool SrcVision::getLeftCameraImage(cv::Mat &img, std_msgs::Header &header)
{
    if (_new_left_camera_image_received == true)
    {
//        cv::flip(_left_camera_image, img, -1);
        img = _left_camera_image.clone();
        header = _left_camera_header;
        _new_left_camera_image_received = false;
        return true;
    }
    else
        return false;
}

void SrcVision::getDisparityImage(cv::Mat &disp_img)
{
    if (_new_disparity_image_received == true)
    {
//        cv::flip(_disparity_image, disp_img, -1);
        disp_img = _disparity_image.clone();
        _new_disparity_image_received = false;
    }
}

cv::Mat_<double> SrcVision::getQMatrix()
{
    return _q_matrix;
}

void SrcVision::get3dImage(cv::Mat &disp, cv::Mat_<double> &q_matrix, cv::Mat &img_3d)
{
    if ((disp.cols > 0) && (disp.rows > 0))
    {
        cv::reprojectImageTo3D(disp, img_3d, q_matrix, false);
//        cv::flip(img_3d, img_3d, -1);
    }
    else
        ROS_WARN_STREAM("Disparity image is empty.");
}

cv::Vec3f SrcVision::get3dPoint(cv::Mat &img_3d, size_t row, size_t col)
{
    cv::Vec3f pt_3d;

    if ((img_3d.rows > 0) && (img_3d.cols > 0))
    {
        // Either (row, col) or (col, row)
        pt_3d = img_3d.at<cv::Vec3f>(cv::Point(col, row));
    }
    else
        ROS_WARN_STREAM("img_3d is empty.");

    return pt_3d;
}

void SrcVision::leftCameraCb(const sensor_msgs::ImageConstPtr &msg)
{
    try
    {
        int source_type = cv_bridge::getCvType(msg->encoding);

        if(source_type == CV_8UC3)
            _cv_bridge = cv_bridge::toCvCopy(msg, sensor_msgs::image_encodings::BGR8);
        else
            _cv_bridge = cv_bridge::toCvCopy(msg, sensor_msgs::image_encodings::MONO8);

        _left_camera_image = _cv_bridge->image.clone();
        _left_camera_header = _cv_bridge->header;
        _new_left_camera_image_received = true;

        ROS_INFO_ONCE("Received new image size: %d x %d", _left_camera_image.rows, _left_camera_image.cols);
     }
     catch (cv_bridge::Exception& e)
     {
        ROS_ERROR_STREAM("Exception: " << e.what());
     }
}

void SrcVision::disparityCb(const stereo_msgs::DisparityImageConstPtr &img)
{
    try
    {
        //the size of the disparity data can be 16 or 32
        uint8_t depth = sensor_msgs::image_encodings::bitDepth(img->image.encoding);

        if (depth == 32)
        {
            cv::Mat_<float> disparity(img->image.height, img->image.width,
                                       const_cast<float*>(reinterpret_cast<const float*>(&img->image.data[0])));

            _disparity_image = disparity.clone();
            _new_disparity_image_received = true;
            _disparity_header = img->image.header;
        }
        else if(depth == 16)
        {
            cv::Mat_<uint16_t> disparityOrigP(img->image.height, img->image.width,
                                              const_cast<uint16_t*>(reinterpret_cast<const uint16_t*>(&img->image.data[0])));
            cv::Mat_<float>   disparity(img->image.height, img->image.width);
            disparity = disparityOrigP / 16.0f;

            _disparity_image = disparity.clone();
            _new_disparity_image_received = true;
            _disparity_header = img->image.header;
        }
        else
            ROS_WARN("Disparity depth not recognized");

        ROS_INFO_ONCE("Received new disparity image size: %d x %d", _disparity_image.rows, _disparity_image.cols);

    }
    catch (std::exception ex)
    {
        ROS_ERROR_STREAM("Exception: " << ex.what());
    }
}
