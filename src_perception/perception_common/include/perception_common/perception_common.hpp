#include <ros/ros.h>
#include <opencv2/opencv.hpp>
#include <cv_bridge/cv_bridge.h>
#include <sensor_msgs/Image.h>
#include <stereo_msgs/DisparityImage.h>

namespace perception_common
{
    namespace TopicNames
    {
        static const std::string LEFT_CAMERA_IMAGE = "/multisense/camera/left/image_rect_color";
        static const std::string DISPARITY_IMAGE = "multisense/camera/disparity";
    }

    class SrcVision
    {
        protected:
            ros::NodeHandle         *_nh;

            cv_bridge::CvImagePtr   _cv_bridge;

            ros::Subscriber         _left_camera_img_sub;
            cv::Mat                 _left_camera_image;
            std_msgs::Header        _left_camera_header;
            bool                    _new_left_camera_image_received;

            cv::Mat_<double>        _q_matrix;

            ros::Subscriber         _disparity_img_sub;
            cv::Mat                 _disparity_image;
            std_msgs::Header        _disparity_header;
            bool                    _new_disparity_image_received;

            void leftCameraCb(const sensor_msgs::ImageConstPtr &msg);
            void disparityCb(const stereo_msgs::DisparityImageConstPtr &msg);

        public:
            SrcVision(ros::NodeHandle *nh);
            ~SrcVision();

            bool getLeftCameraImage(cv::Mat &img, std_msgs::Header &header);
            void getDisparityImage(cv::Mat &disp_img);
            cv::Mat_<double> getQMatrix();

            static void get3dImage(cv::Mat &disp, cv::Mat_<double> &q_matrix, cv::Mat &img_3d);
            static cv::Vec3f get3dPoint(cv::Mat &img_3d, size_t row, size_t col);
    };
}
