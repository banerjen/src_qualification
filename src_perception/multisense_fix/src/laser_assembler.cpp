/**
  * This node takes the different joint states as input and outputs the combined joint states.
  */

#include <ros/ros.h>
#include <csignal>
#include <sensor_msgs/JointState.h>
#include <boost/thread.hpp>
#include <laser_assembler/AssembleScans2.h>
#include <laser_assembler/AssembleScans.h>
#include <laser_geometry/laser_geometry.h>
#include <tf/tf.h>
#include <tf/transform_listener.h>

const std::string SRC_MULTISENSE_JOINT_STATES = "/multisense/joint_states";
const std::string SRC_HARDWARE_JOINT_STATES = "/hardware_joint_states";

ros::Publisher  *g_pub;
sensor_msgs::JointState out_msg;

laser_geometry::LaserProjection projector;
tf::TransformListener           *g_tf_listener;

void shutdown(int signal)
{
    ros::shutdown();
}

void scanCallback(const sensor_msgs::LaserScanConstPtr &scan_in)
{
    sensor_msgs::PointCloud2 cloud;
    projector.projectLaser(*scan_in, cloud);

    g_pub->publish(cloud);
}

void highFidelityScanCallback(const sensor_msgs::LaserScanConstPtr &scan_in)
{
    if (!g_tf_listener->waitForTransform(scan_in->header.frame_id,
                                      "head",
                                      scan_in->header.stamp + ros::Duration().fromSec(scan_in->ranges.size() * scan_in->time_increment),
                                      ros::Duration(1.0)))
    {
        return;
    }

    sensor_msgs::PointCloud2    cloud;
    projector.transformLaserScanToPointCloud("head", *scan_in, cloud, *g_tf_listener);
    g_pub->publish(cloud);
}

int main(int argc, char *argv[])
{
    ros::init(argc, argv, "multisense_laser_assembler_node");
    ros::NodeHandle nh;

    std::signal(SIGINT, shutdown);

    tf::TransformListener tf_listener;

    g_tf_listener = &tf_listener;

    ros::Publisher point_cloud_laser_pub;
    ros::Subscriber scan_sub;

//    scan_sub = nh.subscribe("/multisense/lidar_scan", 1, scanCallback);
    scan_sub = nh.subscribe("/multisense/lidar_scan", 1, highFidelityScanCallback);

    point_cloud_laser_pub = nh.advertise<sensor_msgs::PointCloud2>("/assembled_lidar_cloud", 1, false);

    g_pub = &point_cloud_laser_pub;

    ros::spin();

    return 0;

    ROS_INFO("Waiting for /assemble_scans2 service.");
    ros::service::waitForService("/assemble_scans");

    ros::ServiceClient client = nh.serviceClient<laser_assembler::AssembleScans>("/assemble_scans");
    laser_assembler::AssembleScans srv;

    ROS_INFO("Started /assemble_scans2 service client.");

    ros::Rate rate(10);

    while (ros::ok())
    {
        srv.request.begin = ros::Time(0, 0);
        srv.request.end = ros::Time::now();

        if (client.call(srv))
        {
//            sensor_msgs::PointCloud2 pc2;
//            pc2 = srv.response.cloud;
//            point_cloud_laser_pub.publish(pc2);

            ROS_INFO_STREAM("Call successful. " << srv.response.cloud.points.size());//srv.response.cloud.data.size());
        }
        else
        {
            return 1;
        }

        rate.sleep();
    }

    return 0;
}
