/**
  * This node takes the different joint states as input and outputs the combined joint states.
  */

#include <ros/ros.h>
#include <csignal>
#include <sensor_msgs/JointState.h>
#include <boost/thread.hpp>

const std::string SRC_MULTISENSE_JOINT_STATES = "/multisense/joint_states";
const std::string SRC_HARDWARE_JOINT_STATES = "/hardware_joint_states";

ros::Publisher  *g_pub;
boost::mutex    m_sub_mutex;
sensor_msgs::JointState out_msg;

void shutdown(int signal)
{
//    while (m_sub_mutex.try_lock())
//        m_sub_mutex.unlock();
    ros::shutdown();
//    exit(1);
}

void hardware_callback(const sensor_msgs::JointStateConstPtr &msg)
{
    if (m_sub_mutex.try_lock() == true)
    {
        out_msg.header.frame_id = "world";
        out_msg.header.stamp = msg->header.stamp;
        out_msg.name = msg->name;
        out_msg.position = msg->position;
        out_msg.velocity = msg->velocity;
        out_msg.effort = msg->effort;
        g_pub->publish(out_msg);
        m_sub_mutex.unlock();
        ROS_INFO_ONCE("Finished hardware callback.");
    }
}

void multisense_callback(const sensor_msgs::JointStateConstPtr &msg)
{
    if (m_sub_mutex.try_lock() == true)
    {
        out_msg.header.frame_id = "world";
        out_msg.header.stamp = msg->header.stamp;
        out_msg.name = msg->name;
        out_msg.position = msg->position;
        out_msg.velocity = msg->velocity;
        out_msg.effort = msg->effort;
        g_pub->publish(out_msg);
        m_sub_mutex.unlock();
        ROS_INFO_ONCE("Finished Multisense callback.");
    }    
}

int main(int argc, char *argv[])
{
    ros::init(argc, argv, "multisense_fix_laser");
    ros::NodeHandle nh;

    ros::Subscriber hardware_sub, multisense_sub;
    ros::Publisher  joint_states_pub;

    std::signal(SIGINT, shutdown);

    joint_states_pub = nh.advertise<sensor_msgs::JointState>("joint_states", 1, false);
    g_pub = &joint_states_pub;

    hardware_sub = nh.subscribe(SRC_HARDWARE_JOINT_STATES, 1, hardware_callback);
    multisense_sub = nh.subscribe(SRC_MULTISENSE_JOINT_STATES, 1, multisense_callback);

    ros::spin();

    return 0;
}
