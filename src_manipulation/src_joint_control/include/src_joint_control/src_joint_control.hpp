#pragma once

#include <ros/ros.h>
#include <tf/tf.h>
#include <tf/transform_listener.h>
#include <geometry_msgs/Vector3.h>
#include <geometry_msgs/Quaternion.h>
#include <geometry_msgs/PoseStamped.h>
#include <ihmc_msgs/ArmTrajectoryRosMessage.h>
#include <ihmc_msgs/OneDoFJointTrajectoryRosMessage.h>
#include <ihmc_msgs/TrajectoryPoint1DRosMessage.h>
#include <sensor_msgs/JointState.h>

#define ARRAY_TO_STD_VECTOR(array, vector) \
    if (vector.size() > 0) \
        vector.clear(); \
    vector.assign(array, array + (sizeof(array) / sizeof(array[0])));

namespace src_joint_control
{
    namespace TopicNames
    {
        static const std::string ARM_TRAJECTORY = "/ihmc_ros/valkyrie/control/arm_trajectory";
    }

    namespace Side
    {
        static const uint8_t LEFT = 0;
        static const uint8_t RIGHT = 1;
    }

    namespace LinkName
    {
        static const std::string LEFT_FOOT_FRAME = "leftFoot";
        static const std::string RIGHT_FOOT_FRAME = "rightFoot";
        static const std::string WORLD_FRAME = "world";
    }

    static const double zero_angles[]   = { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0 };
    static const double elbow_bent_up[] = { 0.0, 0.0, 0.0, 2.0, 0.0, 0.0, 0.0 };

    class SrcJointControl
    {
        protected:
            ros::NodeHandle         *_nh;
            tf::TransformListener   *_tf_listener;            

            bool                    _enable_spinner;

            ros::Publisher          _arm_trajectory_pub;
            ros::Subscriber         _joint_states_sub;

            sensor_msgs::JointState right_joint_state_msg_;

            geometry_msgs::Quaternion toQuaternion(const tf::StampedTransform &stamped_tf);
            geometry_msgs::Vector3 toPosition(const tf::StampedTransform &stamped_tf);

            void joint_state_cb(const sensor_msgs::JointStateConstPtr &msg);

        public:
            SrcJointControl(ros::NodeHandle *nh, tf::TransformListener *tf_listener);
            ~SrcJointControl();

            void enableSpinner();
            void disableSpinner();

            void appendTrajectoryPoint(ihmc_msgs::ArmTrajectoryRosMessage &msg,
                                       double time,
                                       std::vector<double> &positions);

            void sendTrajectoryToRobot(const ihmc_msgs::ArmTrajectoryRosMessage &msg);

            void waitForRightArmPosition(const std::vector<double> &arm_pos);
    };
}
