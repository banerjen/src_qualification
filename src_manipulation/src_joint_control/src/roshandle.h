#ifndef ROSHANDLE_H
#define ROSHANDLE_H

#include <cstdio>
#include <cstring>
#include <string>
#include <map>
#include <deque>

#include <QObject>
#include <QtCore/QDebug>

#include <src_joint_control/src_joint_control.hpp>

#include <tf/tf.h>
#include <tf/transform_listener.h>

#include <sensor_msgs/JointState.h>

#ifndef Q_MOC_RUN
#include <ros/ros.h>
#endif

#ifndef Q_MOC_RUN
#include "boost/thread.hpp"
#endif



// RosHandle is the interface with ROS.
class RosHandle : public QObject
{
    Q_OBJECT
    public:
        explicit RosHandle(int argc, char *argv[], QObject *parent = 0);
        std::string getNameSpace();

    public slots:
        void spinOnce();
        void sendJointValsToRobot(std::vector<double> joint_vals);

    private:
        ros::NodeHandle*                            nodeHandle_;
        std::string                                 ns_;
        tf::TransformListener                       *tf_listener;

        ros::Subscriber                             joint_states_sub_;

        boost::mutex                                publisher_mutex_;

        src_joint_control::SrcJointControl          *src_joint_control_;

        sensor_msgs::JointState                     joint_state_msg_;

        void joint_state_cb(const sensor_msgs::JointStateConstPtr &msg);

    signals:
        void updateJointValues(std::vector<double> joint_values);
};

#endif ROSHANDLE_H
