#include <src_joint_control/src_joint_control.hpp>
#include <Eigen/Dense>
#include <tf_conversions/tf_eigen.h>

using namespace src_joint_control;

SrcJointControl::SrcJointControl(ros::NodeHandle *nh, tf::TransformListener *tf_listener): _nh(nh),
                                                                                 _tf_listener(tf_listener),
                                                                                 _enable_spinner(false)
{
    _arm_trajectory_pub = nh->advertise<ihmc_msgs::ArmTrajectoryRosMessage>(TopicNames::ARM_TRAJECTORY, 1, true);
    _joint_states_sub = nh->subscribe("/joint_states", 1, &SrcJointControl::joint_state_cb, this);

    right_joint_state_msg_.name.resize(7);
    right_joint_state_msg_.position.resize(7);
    right_joint_state_msg_.velocity.resize(7);
    right_joint_state_msg_.effort.resize(7);
}

SrcJointControl::~SrcJointControl()
{}

void SrcJointControl::enableSpinner()
{
    _enable_spinner = true;
}

void SrcJointControl::disableSpinner()
{
    _enable_spinner = false;
}

void SrcJointControl::appendTrajectoryPoint(ihmc_msgs::ArmTrajectoryRosMessage &msg,
                                            double time,
                                            std::vector<double> &positions)
{
    if (msg.joint_trajectory_messages.size() == 0)
    {
        for (size_t i = 0; i < positions.size(); ++i)
        {
            ihmc_msgs::OneDoFJointTrajectoryRosMessage one_dof_msg;
            msg.joint_trajectory_messages.push_back(one_dof_msg);
        }
    }

    for (size_t i = 0; i < positions.size(); ++i)
    {
        ihmc_msgs::TrajectoryPoint1DRosMessage point;

        point.time = time;
        point.position = positions[i];
        point.velocity = 0.0;
        msg.joint_trajectory_messages[i].trajectory_points.push_back(point);
    }
}

void SrcJointControl::sendTrajectoryToRobot(const ihmc_msgs::ArmTrajectoryRosMessage &msg)
{
    if (_enable_spinner == true)
        ros::spinOnce();
    ROS_INFO_STREAM("Sending trajectory to the robot.");
    _arm_trajectory_pub.publish(msg);
}

void SrcJointControl::waitForRightArmPosition(const std::vector<double> &arm_pos)
{
    if (arm_pos.size() != 7)
        return;

    bool complete = false;
    size_t num_completions = 0;

    while ((complete == false) && ros::ok())
    {
        num_completions = 0;
        for (size_t i = 0; i < arm_pos.size(); ++i)
        {
            if (std::abs(arm_pos.at(i) - right_joint_state_msg_.position.at(i)) < 0.01)
                ++num_completions;
        }

        if (num_completions == 7)
            return;
    }

    ROS_INFO_STREAM("Completed waiting for right arm position.");
}

geometry_msgs::Quaternion SrcJointControl::toQuaternion(const tf::StampedTransform &stamped_tf)
{
    geometry_msgs::Quaternion ret_q;

    ret_q.x = stamped_tf.getRotation().x();
    ret_q.y = stamped_tf.getRotation().y();
    ret_q.z = stamped_tf.getRotation().z();
    ret_q.w = stamped_tf.getRotation().w();

    return ret_q;
}

geometry_msgs::Vector3 SrcJointControl::toPosition(const tf::StampedTransform &stamped_tf)
{
    geometry_msgs::Vector3 ret_p;

    ret_p.x = stamped_tf.getOrigin().x();
    ret_p.y = stamped_tf.getOrigin().y();
    ret_p.z = stamped_tf.getOrigin().z();

    return ret_p;
}

void SrcJointControl::joint_state_cb(const sensor_msgs::JointStateConstPtr &msg)
{
    for (int i = 0; i < msg->name.size(); ++i)
    {
        if (msg->name[i] == "rightShoulderPitch")
        {
            right_joint_state_msg_.position[0] = msg->position[i];
            right_joint_state_msg_.velocity[0] = msg->velocity[i];
        }
        else if (msg->name[i] == "rightShoulderRoll")
        {
            right_joint_state_msg_.position[1] = msg->position[i];
            right_joint_state_msg_.velocity[1] = msg->velocity[i];
        }
        else if (msg->name[i] == "rightShoulderYaw")
        {
            right_joint_state_msg_.position[2] = msg->position[i];
            right_joint_state_msg_.velocity[2] = msg->velocity[i];
        }
        else if (msg->name[i] == "rightElbowPitch")
        {
            right_joint_state_msg_.position[3] = msg->position[i];
            right_joint_state_msg_.velocity[3] = msg->velocity[i];
        }
        else if (msg->name[i] == "rightForearmYaw")
        {
            right_joint_state_msg_.position[4] = msg->position[i];
            right_joint_state_msg_.velocity[4] = msg->velocity[i];
        }
        else if (msg->name[i] == "rightWristRoll")
        {
            right_joint_state_msg_.position[5] = msg->position[i];
            right_joint_state_msg_.velocity[5] = msg->velocity[i];
        }
        else if (msg->name[i] == "rightWristPitch")
        {
            right_joint_state_msg_.position[6] = msg->position[i];
            right_joint_state_msg_.velocity[6] = msg->velocity[i];
        }
    }
}
