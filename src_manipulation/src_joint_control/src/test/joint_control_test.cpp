#include <src_joint_control/src_joint_control.hpp>
#include <csignal>

void shutdown(int signal)
{
    ros::shutdown();
}

ihmc_msgs::ArmTrajectoryRosMessage createRightArmTrajectory(src_joint_control::SrcJointControl &joint_controller)
{
    ihmc_msgs::ArmTrajectoryRosMessage msg;
    std::vector<double> ZERO_ANGLES, ELBOW_BENT_UP;

    ARRAY_TO_STD_VECTOR(src_joint_control::zero_angles, ZERO_ANGLES);
    ARRAY_TO_STD_VECTOR(src_joint_control::elbow_bent_up, ELBOW_BENT_UP);
    msg.robot_side = ihmc_msgs::ArmTrajectoryRosMessage::RIGHT;
    joint_controller.appendTrajectoryPoint(msg, 2.0, ZERO_ANGLES);
    joint_controller.appendTrajectoryPoint(msg, 3.0, ELBOW_BENT_UP);
    joint_controller.appendTrajectoryPoint(msg, 4.0, ZERO_ANGLES);

    msg.unique_id = -1;

    return msg;
}

int main(int argc, char *argv[])
{
    ros::init(argc, argv, "joint_control_test");
    ros::NodeHandle nh;
    tf::TransformListener tf_listener;
    src_joint_control::SrcJointControl  joint_controller(&nh, &tf_listener);

    std::signal(SIGINT, shutdown);

    for (size_t i = 0; i < 100; ++i)
        ros::spinOnce();

    joint_controller.enableSpinner();

    ihmc_msgs::ArmTrajectoryRosMessage traj_msg;
    traj_msg = createRightArmTrajectory(joint_controller);

    joint_controller.sendTrajectoryToRobot(traj_msg);

    while (ros::ok());

    return 0;
}
