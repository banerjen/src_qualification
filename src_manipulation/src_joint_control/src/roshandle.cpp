#include <roshandle.h>

RosHandle::RosHandle(int argc, char *argv[], QObject *parent)
{
    ros::init(argc, argv, "joint_control_gui");
    nodeHandle_ = new (ros::NodeHandle);

    try
    {
        size_t start_at = 0;
        while (start_at < nodeHandle_->getNamespace().length() && nodeHandle_->getNamespace().at(start_at) == '/')
            start_at++;
        ns_ = nodeHandle_->getNamespace().substr(start_at);
    }
    catch (...)
    {
        ROS_WARN("namespace parsing error");
    }

    src_joint_control_ = new src_joint_control::SrcJointControl(nodeHandle_, tf_listener);

    src_joint_control_->enableSpinner();

    joint_state_msg_.name.push_back("rightShoulderPitch");
    joint_state_msg_.name.push_back("rightShoulderRoll");
    joint_state_msg_.name.push_back("rightShoulderYaw");
    joint_state_msg_.name.push_back("rightElbowPitch");
    joint_state_msg_.name.push_back("rightForearmYaw");
    joint_state_msg_.name.push_back("rightWristRoll");
    joint_state_msg_.name.push_back("rightWristPitch");

    joint_state_msg_.position.resize(7);
    joint_state_msg_.velocity.resize(7);
    joint_state_msg_.effort.resize(7);

    joint_states_sub_ = nodeHandle_->subscribe("/joint_states", 1, &RosHandle::joint_state_cb, this);

    tf_listener = new tf::TransformListener();
}

void RosHandle::spinOnce()
{
    ros::spinOnce();
}

void RosHandle::sendJointValsToRobot(std::vector<double> joint_vals)
{
    ihmc_msgs::ArmTrajectoryRosMessage msg;

    for (size_t i = 0; i < joint_vals.size(); ++i)
    {
        if (joint_vals[i] == 99999)
            joint_vals[i] = joint_state_msg_.position[i];
    }

    msg.robot_side = ihmc_msgs::ArmTrajectoryRosMessage::RIGHT;
    src_joint_control_->appendTrajectoryPoint(msg, 2.0, joint_vals);

    msg.unique_id = -1;

    src_joint_control_->sendTrajectoryToRobot(msg);
}

std::string RosHandle::getNameSpace()
{
    return ns_;
}

void RosHandle::joint_state_cb(const sensor_msgs::JointStateConstPtr &msg)
{
    for (int i = 0; i < msg->name.size(); ++i)
    {
        if (msg->name[i] == "rightShoulderPitch")
        {
            joint_state_msg_.position[0] = msg->position[i];
            joint_state_msg_.velocity[0] = msg->velocity[i];
        }
        else if (msg->name[i] == "rightShoulderRoll")
        {
            joint_state_msg_.position[1] = msg->position[i];
            joint_state_msg_.velocity[1] = msg->velocity[i];
        }
        else if (msg->name[i] == "rightShoulderYaw")
        {
            joint_state_msg_.position[2] = msg->position[i];
            joint_state_msg_.velocity[2] = msg->velocity[i];
        }
        else if (msg->name[i] == "rightElbowPitch")
        {
            joint_state_msg_.position[3] = msg->position[i];
            joint_state_msg_.velocity[3] = msg->velocity[i];
        }
        else if (msg->name[i] == "rightForearmYaw")
        {
            joint_state_msg_.position[4] = msg->position[i];
            joint_state_msg_.velocity[4] = msg->velocity[i];
        }
        else if (msg->name[i] == "rightWristRoll")
        {
            joint_state_msg_.position[5] = msg->position[i];
            joint_state_msg_.velocity[5] = msg->velocity[i];
        }
        else if (msg->name[i] == "rightWristPitch")
        {
            joint_state_msg_.position[6] = msg->position[i];
            joint_state_msg_.velocity[6] = msg->velocity[i];
        }
    }

    std::vector<double> joint_vals;
    joint_vals = joint_state_msg_.position;
    emit updateJointValues(joint_vals);

}

//['rightShoulderPitch', 'rightShoulderRoll', 'rightShoulderYaw', 'rightElbowPitch', 'rightForearmYaw', 'rightWristRoll', 'rightWristPitch']
