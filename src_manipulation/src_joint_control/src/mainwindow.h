#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QSettings>
#include <QSpacerItem>
#include "QPushButton"
#include "QListWidget"

#include "vector"

namespace Ui {
class MainWindow;
}

//! MainWindow UI
/*!
  The main window is the container for all subwidgets.
  Configurations and session settings are saved/restored from here.
  \param parent is the parent (if any) of the mainwindow
*/
class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QString name = "", QWidget *parent = 0);
    ~MainWindow();
    void restoreSession();
    void saveSession();

public slots:
    void updateJointValues(std::vector<double> joint_vals);

private:
    Ui::MainWindow  *ui;
    QString         _fileName;
    QString         _namespace;
    QSettings *     _sessionSettings;

    bool            paused;

signals:
    void timerEventSIGNAL();
    void sendJointVals(std::vector<double> joint_vals);

protected:
     void timerEvent(QTimerEvent *event);
     void closeEvent(QCloseEvent *event);
private slots:

     void on_pushButton_accept_clicked();
};

#endif // MAINWINDOW_H
