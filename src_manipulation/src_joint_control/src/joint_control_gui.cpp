#include "QDebug"
#include "QApplication"
#include "QSettings"

#include "mainwindow.h"
#include "roshandle.h"

#include "ros/ros.h"
#include "csignal"

#include "boost/thread.hpp"

MainWindow* mainWindow = NULL;

void mySigintHandler(int sig)
{
    if (mainWindow)
        mainWindow->close();

    ros::shutdown();
}

int main(int argc, char *argv[])
{
//    Q_INIT_RESOURCE(resources);
    QCoreApplication::setOrganizationName("NandanBanerjee");
    QCoreApplication::setOrganizationDomain("www.nandanbanerjee.com");
    QCoreApplication::setApplicationName("Joint Control Gui");
//    QCoreApplication::setAttribute(Qt::AA_DontUseNativeMenuBar);

    QApplication app(argc, argv);

    RosHandle * rosHandle = new RosHandle(argc, argv);
    mainWindow = new MainWindow(QString::fromStdString(rosHandle->getNameSpace()));
//    mw = &mainWindow;

    // Override the default ros sigint handler.
    // This must be set after the first NodeHandle is created.
    std::signal(SIGINT, mySigintHandler);

    // Restore Session Settings:
    mainWindow->restoreSession();

    mainWindow->connect(mainWindow, SIGNAL(timerEventSIGNAL()),               rosHandle,  SLOT(spinOnce()));
    mainWindow->connect(rosHandle, SIGNAL(updateJointValues(std::vector<double>)), mainWindow, SLOT(updateJointValues(std::vector<double>)));
    mainWindow->connect(mainWindow, SIGNAL(sendJointVals(std::vector<double>)), rosHandle, SLOT(sendJointValsToRobot(std::vector<double>)));

    mainWindow->show();

    return app.exec();
}
