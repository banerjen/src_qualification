#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QtCore/QTimer>
#include <QDir>
#include <QLabel>
#include <QLineEdit>
#include <QFileDialog>
#include <QtCore/QDebug>
#include <QComboBox>
#include <QListWidget>
#include <iostream>

MainWindow::MainWindow(QString name, QWidget *parent):
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    _namespace = name;
    if (_namespace.isEmpty())
        _namespace = QString("Joint Control Gui");
    else
        _namespace = QString("  Joint Control Gui - ") + _namespace;

    setWindowTitle(_namespace);

    // loop rate at 30 Hz
    this->startTimer(1000/30);

    QDir dir;
    _fileName = dir.homePath()+"/.config/SRC/"+_namespace+".conf";
    _sessionSettings = new QSettings(_fileName,QSettings::IniFormat);
}

void MainWindow::closeEvent(QCloseEvent *event)
{
    saveSession();
    QMainWindow::closeEvent(event);
}

void MainWindow::restoreSession()
{
    this->restoreGeometry(_sessionSettings->value("mainwindow/geometry").toByteArray());
    this->restoreState(_sessionSettings->value("mainwindow/windowState").toByteArray());
}

void MainWindow::saveSession()
{
    _sessionSettings->clear();
    _sessionSettings->setValue("mainwindow/geometry", saveGeometry());
    _sessionSettings->setValue("mainwindow/windowState", saveState());
    _sessionSettings->sync();
}

void MainWindow::timerEvent(QTimerEvent *event)
{
    emit timerEventSIGNAL();
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::updateJointValues(std::vector<double> joint_vals)
{
    if (joint_vals.size() < 7)
        return;

    ui->label->setText(QString("Joint 1   ") + QString::number(joint_vals[0], 'f', 3));
    ui->label_2->setText(QString("Joint 2   ") + QString::number(joint_vals[1], 'f', 3));
    ui->label_3->setText(QString("Joint 3   ") + QString::number(joint_vals[2], 'f', 3));
    ui->label_4->setText(QString("Joint 4   ") + QString::number(joint_vals[3], 'f', 3));
    ui->label_5->setText(QString("Joint 5   ") + QString::number(joint_vals[4], 'f', 3));
    ui->label_6->setText(QString("Joint 6   ") + QString::number(joint_vals[5], 'f', 3));
    ui->label_7->setText(QString("Joint 7   ") + QString::number(joint_vals[6], 'f', 3));
}

void MainWindow::on_pushButton_accept_clicked()
{
    std::vector<double> joint_vals;

    if (ui->lineEdit->text() == QString(""))
        joint_vals.push_back(99999);
    else
        joint_vals.push_back(ui->lineEdit->text().toDouble());

    if (ui->lineEdit_2->text() == QString(""))
        joint_vals.push_back(99999);
    else
        joint_vals.push_back(ui->lineEdit_2->text().toDouble());

    if (ui->lineEdit_3->text() == QString(""))
        joint_vals.push_back(99999);
    else
        joint_vals.push_back(ui->lineEdit_3->text().toDouble());

    if (ui->lineEdit_4->text() == QString(""))
        joint_vals.push_back(99999);
    else
        joint_vals.push_back(ui->lineEdit_4->text().toDouble());

    if (ui->lineEdit_5->text() == QString(""))
        joint_vals.push_back(99999);
    else
        joint_vals.push_back(ui->lineEdit_5->text().toDouble());

    if (ui->lineEdit_6->text() == QString(""))
        joint_vals.push_back(99999);
    else
        joint_vals.push_back(ui->lineEdit_6->text().toDouble());

    if (ui->lineEdit_7->text() == QString(""))
        joint_vals.push_back(99999);
    else
        joint_vals.push_back(ui->lineEdit_7->text().toDouble());

    emit sendJointVals(joint_vals);
}
