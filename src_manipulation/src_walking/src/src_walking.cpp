#include <src_walking/src_walking.hpp>
#include <Eigen/Dense>
#include <tf_conversions/tf_eigen.h>

using namespace src_walking;

SrcWalking::SrcWalking(ros::NodeHandle *nh, tf::TransformListener *tf_listener): _nh(nh),
                                                                                 _tf_listener(tf_listener),
                                                                                 _step_counter(0),
                                                                                 _enable_spinner(false)
{
    _footstep_list_pub = nh->advertise<ihmc_msgs::FootstepDataListRosMessage>(TopicNames::FOOTSTEP_LIST,
                                                                              1,
                                                                              true);
    _footstep_status_sub = nh->subscribe(TopicNames::FOOTSTEP_STATUS, 5, &SrcWalking::receivedFootStepStatus, this);
}

SrcWalking::~SrcWalking()
{}

ihmc_msgs::FootstepDataRosMessage SrcWalking::createFootStepInPlace(uint8_t step_side)
{
    ihmc_msgs::FootstepDataRosMessage footstep;
    std::string foot_frame;
    tf::StampedTransform stamped_tf;

    if (step_side == Side::LEFT)
    {
        footstep.robot_side = ihmc_msgs::FootstepDataRosMessage::LEFT;
        foot_frame = LinkName::LEFT_FOOT_FRAME;
    }
    else if (step_side == Side::RIGHT)
    {
        footstep.robot_side = ihmc_msgs::FootstepDataRosMessage::RIGHT;
        foot_frame = LinkName::RIGHT_FOOT_FRAME;
    }

    while ((_tf_listener->waitForTransform(LinkName::WORLD_FRAME, foot_frame, ros::Time(0), ros::Duration(3.0)) == false) && (ros::ok()));
    _tf_listener->lookupTransform(LinkName::WORLD_FRAME, foot_frame, ros::Time(0), stamped_tf);
    footstep.orientation = toQuaternion(stamped_tf);
    footstep.location = toPosition(stamped_tf);

    return footstep;
}

ihmc_msgs::FootstepDataRosMessage SrcWalking::createFootStepOffset(uint8_t step_side, geometry_msgs::Vector3 offset)
{
    ihmc_msgs::FootstepDataRosMessage footstep;
    geometry_msgs::Quaternion quat;
    Eigen::Matrix3d m_rot;
    Eigen::Vector3d v_transformed_pos;
    Eigen::Vector3d v_offset;

    footstep = createFootStepInPlace(step_side);

    /// Transform the offset to world frame
    quat = footstep.orientation;
    Eigen::Quaterniond eigen_quat(quat.w, quat.x, quat.y, quat.z);
    m_rot = eigen_quat;
    v_offset[0] = offset.x;
    v_offset[1] = offset.y;
    v_offset[2] = offset.z;
    v_transformed_pos = m_rot * v_offset;

    footstep.location.x = footstep.location.x + v_transformed_pos[0];
    footstep.location.y = footstep.location.y + v_transformed_pos[1];
    footstep.location.z = footstep.location.z + v_transformed_pos[2];

    return footstep;
}

void SrcWalking::startWalking(const ihmc_msgs::FootstepDataListRosMessage &msg)
{
    this->_footstep_list_pub.publish(msg);
    ROS_INFO_STREAM("Walking.");
    this->waitForFootSteps(msg.footstep_data_list.size());
}

void SrcWalking::enableSpinner()
{
    _enable_spinner = true;
}

void SrcWalking::disableSpinner()
{
    _enable_spinner = false;
}

geometry_msgs::Vector3 SrcWalking::toVector3(double x, double y, double z)
{
    geometry_msgs::Vector3 ret_v;

    ret_v.x = x;
    ret_v.y = y;
    ret_v.z = z;

    return ret_v;
}

geometry_msgs::Quaternion SrcWalking::toQuaternion(const tf::StampedTransform &stamped_tf)
{
    geometry_msgs::Quaternion ret_q;

    ret_q.x = stamped_tf.getRotation().x();
    ret_q.y = stamped_tf.getRotation().y();
    ret_q.z = stamped_tf.getRotation().z();
    ret_q.w = stamped_tf.getRotation().w();

    return ret_q;
}

geometry_msgs::Vector3 SrcWalking::toPosition(const tf::StampedTransform &stamped_tf)
{
    geometry_msgs::Vector3 ret_p;

    ret_p.x = stamped_tf.getOrigin().x();
    ret_p.y = stamped_tf.getOrigin().y();
    ret_p.z = stamped_tf.getOrigin().z();

    return ret_p;
}

void SrcWalking::waitForFootSteps(uint32_t num_footsteps)
{
    ros::Rate rate(10);
    _step_counter = 0;

    while ((_step_counter < num_footsteps) && (ros::ok()))
    {
        if (_enable_spinner == true)
            ros::spinOnce();
        rate.sleep();
    }

    ROS_INFO_STREAM("Finished set of steps.");
}

void SrcWalking::receivedFootStepStatus(const ihmc_msgs::FootstepStatusRosMessageConstPtr &msg)
{
    if (msg->status == ihmc_msgs::FootstepStatusRosMessage::COMPLETED)
    {
        if (_enable_spinner == true)
            ros::spinOnce();
        ++_step_counter;
        std::cout << "Finished step " << _step_counter << std::endl;
    }
}
