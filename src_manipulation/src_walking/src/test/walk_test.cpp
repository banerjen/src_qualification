#include <src_walking/src_walking.hpp>
#include <csignal>

void shutdown(int signal)
{
    ros::shutdown();
}

ihmc_msgs::FootstepDataListRosMessage createFootsteps(src_walking::SrcWalking &val_walking)
{
    ihmc_msgs::FootstepDataListRosMessage msg;
    msg.transfer_time = 1.5;
    msg.swing_time = 1.5;
    msg.execution_mode = 0;
    msg.unique_id = -1;


    msg.footstep_data_list.push_back(val_walking.createFootStepOffset(src_walking::Side::LEFT,
                                                                      src_walking::SrcWalking::toVector3(0.2, 0.0, 0.0)));
    msg.footstep_data_list.push_back(val_walking.createFootStepOffset(src_walking::Side::RIGHT,
                                                                      src_walking::SrcWalking::toVector3(0.4, 0.0, 0.0)));
    msg.footstep_data_list.push_back(val_walking.createFootStepOffset(src_walking::Side::LEFT,
                                                                      src_walking::SrcWalking::toVector3(0.6, 0.0, 0.0)));
    msg.footstep_data_list.push_back(val_walking.createFootStepOffset(src_walking::Side::RIGHT,
                                                                      src_walking::SrcWalking::toVector3(0.8, 0.0, 0.0)));
    msg.footstep_data_list.push_back(val_walking.createFootStepOffset(src_walking::Side::LEFT,
                                                                      src_walking::SrcWalking::toVector3(1.0, 0.0, 0.0)));
    msg.footstep_data_list.push_back(val_walking.createFootStepOffset(src_walking::Side::RIGHT,
                                                                      src_walking::SrcWalking::toVector3(1.2, 0.0, 0.0)));
    msg.footstep_data_list.push_back(val_walking.createFootStepOffset(src_walking::Side::LEFT,
                                                                      src_walking::SrcWalking::toVector3(1.4, 0.0, 0.0)));
    msg.footstep_data_list.push_back(val_walking.createFootStepOffset(src_walking::Side::RIGHT,
                                                                      src_walking::SrcWalking::toVector3(1.6, 0.0, 0.0)));
    msg.footstep_data_list.push_back(val_walking.createFootStepOffset(src_walking::Side::LEFT,
                                                                      src_walking::SrcWalking::toVector3(1.8, 0.0, 0.0)));
    msg.footstep_data_list.push_back(val_walking.createFootStepOffset(src_walking::Side::RIGHT,
                                                                      src_walking::SrcWalking::toVector3(2.0, 0.0, 0.0)));
    msg.footstep_data_list.push_back(val_walking.createFootStepOffset(src_walking::Side::LEFT,
                                                                      src_walking::SrcWalking::toVector3(2.2, 0.0, 0.0)));
    msg.footstep_data_list.push_back(val_walking.createFootStepOffset(src_walking::Side::RIGHT,
                                                                      src_walking::SrcWalking::toVector3(2.4, 0.0, 0.0)));

    return msg;
}

int main(int argc, char *argv[])
{
    ros::init(argc, argv, "walk_test");
    ros::NodeHandle nh;
    tf::TransformListener tf_listener;
    src_walking::SrcWalking val_walking(&nh, &tf_listener);
    ihmc_msgs::FootstepDataListRosMessage footstep_msg;

    std::signal(SIGINT, shutdown);

    for (size_t i = 0; i < 100; ++i)
        ros::spinOnce();

    footstep_msg = createFootsteps(val_walking);
    val_walking.enableSpinner();
    val_walking.startWalking(footstep_msg);

    return 0;
}
