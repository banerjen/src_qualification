#pragma once

#include <ros/ros.h>
#include <tf/tf.h>
#include <tf/transform_listener.h>
#include <geometry_msgs/Vector3.h>
#include <geometry_msgs/Quaternion.h>
#include <ihmc_msgs/FootstepStatusRosMessage.h>
#include <ihmc_msgs/FootstepDataListRosMessage.h>
#include <ihmc_msgs/FootstepDataRosMessage.h>

namespace src_walking
{
    namespace TopicNames
    {
        static const std::string FOOTSTEP_STATUS = "/ihmc_ros/valkyrie/output/footstep_status";
        static const std::string FOOTSTEP_LIST = "/ihmc_ros/valkyrie/control/footstep_list";
    }

    namespace Side
    {
        static const uint8_t LEFT = 0;
        static const uint8_t RIGHT = 1;
    }

    namespace LinkName
    {
        static const std::string LEFT_FOOT_FRAME = "leftFoot";
        static const std::string RIGHT_FOOT_FRAME = "rightFoot";
        static const std::string WORLD_FRAME = "world";
    }

    class SrcWalking
    {
        protected:
            ros::NodeHandle         *_nh;
            tf::TransformListener   *_tf_listener;

            uint32_t                _step_counter;
            ros::Publisher          _footstep_list_pub;
            ros::Subscriber         _footstep_status_sub;

            bool                    _enable_spinner;

            geometry_msgs::Quaternion toQuaternion(const tf::StampedTransform &stamped_tf);
            geometry_msgs::Vector3 toPosition(const tf::StampedTransform &stamped_tf);

            void waitForFootSteps(uint32_t num_footsteps);
            void receivedFootStepStatus(const ihmc_msgs::FootstepStatusRosMessageConstPtr &msg);

        public:
            SrcWalking(ros::NodeHandle *nh, tf::TransformListener *tf_listener);
            ~SrcWalking();

            ihmc_msgs::FootstepDataRosMessage createFootStepInPlace(uint8_t step_side);
            ihmc_msgs::FootstepDataRosMessage createFootStepOffset(uint8_t step_side, geometry_msgs::Vector3 offset);

            void startWalking(const ihmc_msgs::FootstepDataListRosMessage &msg);
            void enableSpinner();
            void disableSpinner();

            static geometry_msgs::Vector3 toVector3(double x, double y, double z);
    };
}
