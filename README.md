# SRC Qualification Codebase (Team Auxilium) [Working, did not submit because of competition citizenship requirements]
***

## Team ##

* Nandan Banerjee

## Resources related to the SRC ##

[ SRC website ](https://ninesights.ninesigma.com/web/space-robotics-challenge)

[ Rulebook ](https://ninesights.ninesigma.com/documents/15108047/0/Rules+doc+9.1.16/da198900-c69e-42b5-8478-fab2c50ebe0e)

[ Valkyrie specifications ](http://nasa-jsc-robotics.github.io/valkyrie/)

[Contact NASA about SRC - Kim Hambuchen - kimberly.a.hambuchen@nasa.gov](mailto:kimberly.a.hambuchen@nasa.gov)

## Qualification Tasks ##

[Qual 1 Task](https://bitbucket.org/osrf/srcsim/wiki/qual_task1)

[Qual 2 Task](https://bitbucket.org/osrf/srcsim/wiki/qual_task2)

## Important Dates ##

   -         **`10-07-2016 - Deadline for registration`**

   -         **`10-10-2016 - Software for qualification (SRC Software Version 1)`**

   -         `12-21-2016 - Qualification deadline`

## System Requirements ##

   -         Ubuntu 14.04
   -         NVIDIA Graphics card with drivers installed
   -         Hyperthreading disabled in the BIOS

## Setting up a system to run SRCSim ##

This tutorial will walk you through the setup required to make a computer ready to run SRCSim. For more information about recommended and minimum requirements see the [System Requirements](https://bitbucket.org/osrf/srcsim/wiki/system_requirements) page.

1. Download and install ROS Indigo (http://wiki.ros.org/indigo/Installation/Ubuntu)

    ```
sudo sh -c 'echo "deb http://packages.ros.org/ros/ubuntu $(lsb_release -sc) main" > /etc/apt/sources.list.d/ros-latest.list'
    ```

    ```
sudo apt-key adv --keyserver hkp://ha.pool.sks-keyservers.net --recv-key 0xB01FA116
    ```

    ```
sudo apt-get update
    ```

    ```
sudo apt-get install ros-indigo-desktop-full
    ```

    ```
sudo rosdep init
    ```

    ```
rosdep update
    ```

    ```
echo "source /opt/ros/indigo/setup.bash" >> ~/.bashrc
    ```

    ```
source ~/.bashrc
    ```

1. Install the Gazebo package repository

    1. Add the Gazebo7 repository. Open a new terminal and type:

        ```
sudo sh -c 'echo "deb http://packages.osrfoundation.org/gazebo/ubuntu-stable `lsb_release -cs` main" > /etc/apt/sources.list.d/gazebo-stable.list'
        ```

    1. Download the signing key

        ```
wget -O - http://packages.osrfoundation.org/gazebo.key | sudo apt-key add -
        ```

1. Install the SRCSim package repository

    1. Add the SRCSim repository

        ```
sudo sh -c 'echo "deb http://srcsim.gazebosim.org/src trusty main" \
    > /etc/apt/sources.list.d/src-latest.list'
        ```

    1. Download the signing keys

        ```
wget -O - http://srcsim.gazebosim.org/src/src.key | sudo apt-key add -
        ```

        ```
wget -O - https://bintray.com/user/downloadSubjectPublicKey?username=bintray | sudo apt-key add -
        ```

1. Update the `apt` database

    ```
sudo apt-get update
    ```

1. Install the SRCSim package and all its dependencies

    ```
sudo apt-get install srcsim
    ```

1. Update your `JAVA_HOME` environment variable

    ```
echo 'export JAVA_HOME=/usr/lib/jvm/java-8-openjdk-amd64' >> ~/.bashrc
    ```

1. Add `IS_GAZEBO` environment variable:

    ```
echo 'export IS_GAZEBO=true' >> ~/.bashrc
    ```


1. Change ownership of `ihmc_ros_java_adapter`. This ROS package requires to write some files in its installation directory at runtime. We're working on a fix for this issue. In the meantime, please change the ownership of this directory to your user.

    ```
sudo chown -R $USER:$USER /opt/ros/indigo/share/ihmc_ros_java_adapter
    ```

1. Copy the IHMC networking `ini` file 


    ```
mkdir -p ${HOME}/.ihmc; curl https://raw.githubusercontent.com/ihmcrobotics/ihmc_ros_core/0.8.0/ihmc_ros_common/configurations/IHMCNetworkParametersTemplate.ini > ${HOME}/.ihmc/IHMCNetworkParameters.ini
    ```

1. Increase real-time scheduling priority for current user (rtprio), which is required by the IHMC controller. Add current user to ros group:

    ```
sudo bash -c 'echo "@ros    -       rtprio      99" > /etc/security/limits.d/ros-rtprio.conf'
    ```

    ```
sudo groupadd ros
    ```

    ```
sudo usermod -a -G ros $USER
    ```

    **Logout from your current session and log in to make sure that all these changes are in place.**

1. Download the deployed Valkyrie controller

    ```
wget -P /tmp/ http://gazebosim.org/distributions/srcsim/valkyrie_controller.tar.gz
    ```

    ```
tar -xvf /tmp/valkyrie_controller.tar.gz -C $HOME
    ```

    ```
rm /tmp/valkyrie_controller.tar.gz
    ```

1. Download all the required Gazebo models

    ```
wget -P /tmp/ https://bitbucket.org/osrf/gazebo_models/get/default.tar.gz
    ```

    ```
tar -xvf /tmp/default.tar.gz -C $HOME/.gazebo/models --strip 1
    ```

    ```
rm /tmp/default.tar.gz
    ```

1. Clone IHMC's open robotics software repo and compile the Java code.

    ```
cd ~
    ```

    ```
git clone https://github.com/ihmcrobotics/ihmc-open-robotics-software.git
    ```

    ```
cd ihmc-open-robotics-software
    ```

    ```
git checkout master
    ```

    ```
./gradlew -q deployLocal
    ```

1. Pre-build `ihmc_ros_java_adapter`. Open a new terminal and run:

    ```
source /opt/nasa/indigo/setup.bash
    ```

    ```
roslaunch ihmc_valkyrie_ros valkyrie_warmup_gradle_cache.launch
    ```

## Test your installation

1. Open a new terminal and type:

    ```
source /opt/nasa/indigo/setup.bash
    ```

    ```
roslaunch srcsim qual2.launch init:=true walk_test:=true
    ```

You should see your robot appear into the Gazebo scene. After a few seconds, the robot should approach the ground, switch to high level control, detach from the virtual harness and start walking.

Here's a [video](https://vimeo.com/188873182) of the expected behavior.

## Getting started with the codebase ##

The qual1 and the qual2 tasks have been completed and their solution code can be found inside "src_tasks/qual1" and "src_tasks/qual2" folders in the master branch.

+ Create a catkin workspace in your home directory ('~/').

      -   `mkdir -p src_catkin_workspace/src/`

      -   `cd src_catkin_workspace/src/`

      -   `git clone https://$user$@bitbucket.org/banerjen/src_qualification.git`

      -   `cd ..`

      -   `catkin_make`

      -   `echo "source ~/src_catkin_workspace/devel/setup.bash" >> ~/.bashrc`

## ##

+ To run the qual1 task, run the following in 3 different terminals (wait for the robot to switch to IHMC's controller and stabilize itself after running qual1.launch) -

      -   `roslaunch srcsim qual1.launch extra_gazebo_args:="-r" init:="true"`

      -   `roslaunch src_launch src_auxilium.launch`

      -   `rosrun qual1 qual1`
## ##

+ To run the qual2 task, run the following in 3 different terminals (wait for the robot to stabilize after running qual2.launch) -

      -   `roslaunch srcsim qual2.launch init:="true"`

      -   `roslaunch src_launch src_auxilium.launch`

      -   `rosrun qual2 qual2`
## ##

![Qual1 completed](images/qual1_completed.png)
![Qual2 completed](images/qual2_completed.png)