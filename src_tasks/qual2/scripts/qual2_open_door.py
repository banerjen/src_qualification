#!/usr/bin/env python

import copy
import time
import rospy

from numpy import append

from ihmc_msgs.msg import ArmTrajectoryRosMessage
from ihmc_msgs.msg import OneDoFJointTrajectoryRosMessage
from ihmc_msgs.msg import TrajectoryPoint1DRosMessage

in_front_of_btn = [0.42110520601272583, 0.39039456844329834, -0.47872865200042725, 1.259277582168579, 0.00019412707479204983, 0.0009970464743673801, -0.00035683365422300994]
button_press = [-0.7671430706977844, 0.48597434163093567, 0.28328609466552734, 0.8378027081489563, -0.0000031809531, 0.0008243864285759628, -0.0006402413127943873]
button_press2 = [-0.7671430706977844, 0.78597434163093567, 0.48328609466552734, 0.5378027081489563, -0.0000031809531, 0.0008243864285759628, -0.0006402413127943873]
intermid_position = [1.1742092370986938, 0.4755037724971771, -1.308236002922058, 1.4551059007644653, 0.00034577518817968667, 0.0010436400771141052, -0.00019693597278092057]
init_position = [-0.19988882541656494, 1.2085695266723633, 0.7099692225456238, 1.509692668914795, -0.0002945896703749895, 0.0004518383357208222, -0.0010168595472350717]

ROBOT_NAME = None

def sendRightArmTrajectory():
    msg = ArmTrajectoryRosMessage()

    msg.robot_side = ArmTrajectoryRosMessage.RIGHT

    #msg = appendTrajectoryPoint(msg, 2.0, intermid_position)
    msg = appendTrajectoryPoint(msg, 2.0, in_front_of_btn)
    # msg = appendTrajectoryPoint(msg, 2.0, button_press)
    # msg = appendTrajectoryPoint(msg, 3.0, button_press2)
    #msg = appendTrajectoryPoint(msg, 2.0, in_front_of_btn)   # may want to remove this to speed up
    #msg = appendTrajectoryPoint(msg, 2.0, intermid_position) # may want to remove this to speed up
    #msg = appendTrajectoryPoint(msg, 2.0, init_position)

    msg.unique_id = -1

    rospy.loginfo('publishing right trajectory')
    armTrajectoryPublisher.publish(msg)

def appendTrajectoryPoint(arm_trajectory, time, positions):
    if not arm_trajectory.joint_trajectory_messages:
        arm_trajectory.joint_trajectory_messages = [copy.deepcopy(OneDoFJointTrajectoryRosMessage()) for i in range(len(positions))]
    for i, pos in enumerate(positions):
        point = TrajectoryPoint1DRosMessage()
        point.time = time
        point.position = pos
        point.velocity = 0
        arm_trajectory.joint_trajectory_messages[i].trajectory_points.append(point)
    return arm_trajectory

if __name__ == '__main__':
    try:
        rospy.init_node('ihmc_arm_press_button')

        ROBOT_NAME = rospy.get_param('/ihmc_ros/robot_name')

        armTrajectoryPublisher = rospy.Publisher("/ihmc_ros/{0}/control/arm_trajectory".format(ROBOT_NAME), ArmTrajectoryRosMessage, queue_size=1)

        rate = rospy.Rate(10) # 10hz
        time.sleep(1)

        # make sure the simulation is running otherwise wait
        if armTrajectoryPublisher.get_num_connections() == 0:
            rospy.loginfo('waiting for subscriber...')
            while armTrajectoryPublisher.get_num_connections() == 0:
                rate.sleep()

        if not rospy.is_shutdown():
            sendRightArmTrajectory()
            time.sleep(2)

    except rospy.ROSInterruptException:
        pass
