/**
  * This node contains the code for performing the qual2 task.
  */

#include <ros/ros.h>
#include <csignal>
#include <boost/thread.hpp>
#include <stack>
#include <geometry_msgs/PoseStamped.h>
#include <ihmc_msgs/ArmTrajectoryRosMessage.h>
#include <ihmc_msgs/OneDoFJointTrajectoryRosMessage.h>
#include <ihmc_msgs/TrajectoryPoint1DRosMessage.h>

#include <ihmc_msgs/HandTrajectoryRosMessage.h>

#include <tf/tf.h>
#include <tf/transform_listener.h>

#include <Eigen/Dense>

#define ARRAY_TO_STD_VECTOR(array, vector) \
    if (vector.size() > 0) \
        vector.clear(); \
    vector.assign(array, array + (sizeof(array) / sizeof(array[0])));

static const double zero_angles[]   = { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0 };
static const double elbow_bent_up[] = { 0.0, 0.0, 0.0, 2.0, 0.0, 0.0, 0.0 };

ros::Publisher *g_hand_pub;

void shutdown(int signal)
{
    ros::shutdown();
}

geometry_msgs::Pose getCurrentHandPositionWrtWorld(tf::TransformListener &tf_listener)
{
    tf::StampedTransform stamped_tf;
    geometry_msgs::Pose hand_pose;

    while ((tf_listener.waitForTransform("world", "rightPalm", ros::Time(0), ros::Duration(3.0)) == false) && (ros::ok()));
    tf_listener.lookupTransform("world", "rightPalm", ros::Time(0), stamped_tf);
    hand_pose.position.x = stamped_tf.getOrigin().x();
    hand_pose.position.y = stamped_tf.getOrigin().y();
    hand_pose.position.z = stamped_tf.getOrigin().z();
    hand_pose.orientation.w = stamped_tf.getRotation().w();
    hand_pose.orientation.x = stamped_tf.getRotation().x();
    hand_pose.orientation.y = stamped_tf.getRotation().y();
    hand_pose.orientation.z = stamped_tf.getRotation().z();

    ROS_INFO("Constructed current hand pose wrt world.");

    return hand_pose;
}

ihmc_msgs::HandTrajectoryRosMessage setPushButtonHandMessage(tf::TransformListener &tf_listener)
{
    ihmc_msgs::HandTrajectoryRosMessage msg;
    ihmc_msgs::SE3TrajectoryPointRosMessage taskspace_msg;
    geometry_msgs::Pose cur_hand_pose;

    cur_hand_pose = getCurrentHandPositionWrtWorld(tf_listener);

    std::cout << "cur_hand_pose.x : " << cur_hand_pose.position.x << std::endl;
    std::cout << "cur_hand_pose.y : " << cur_hand_pose.position.y << std::endl;
    std::cout << "cur_hand_pose.z : " << cur_hand_pose.position.z << std::endl;

    Eigen::Quaterniond eigen_quat(cur_hand_pose.orientation.w,
                                  cur_hand_pose.orientation.x,
                                  cur_hand_pose.orientation.y,
                                  cur_hand_pose.orientation.z);

    Eigen::Matrix3d m_rot;

    m_rot = eigen_quat;

    std::cout << m_rot << std::endl;

    Eigen::Quaterniond eigen_quat2(1,
                                  0,
                                  0,
                                  0);
    m_rot = eigen_quat2;
    std::cout << std::endl;
    std::cout << m_rot << std::endl;
//    exit(0);

    msg.robot_side = ihmc_msgs::HandTrajectoryRosMessage::RIGHT;
    msg.base_for_control = ihmc_msgs::HandTrajectoryRosMessage::WORLD;

    taskspace_msg.time = 1.0;
    taskspace_msg.position.x = cur_hand_pose.position.x + 0.2;
    taskspace_msg.position.y = cur_hand_pose.position.y - 0.0;
    taskspace_msg.position.z = cur_hand_pose.position.z + 0.0;
    taskspace_msg.orientation.w = cur_hand_pose.orientation.w;
    taskspace_msg.orientation.x = cur_hand_pose.orientation.x;
    taskspace_msg.orientation.y = cur_hand_pose.orientation.y;
    taskspace_msg.orientation.z = cur_hand_pose.orientation.z;
    taskspace_msg.linear_velocity.x = 0.01;
    taskspace_msg.linear_velocity.y = 0.01;
    taskspace_msg.linear_velocity.z = 0.01;
    taskspace_msg.angular_velocity.x = 0.01;
    taskspace_msg.angular_velocity.y = 0.01;
    taskspace_msg.angular_velocity.z = 0.01;
    taskspace_msg.unique_id = 72;

    msg.taskspace_trajectory_points.push_back(taskspace_msg);
    msg.execution_mode = ihmc_msgs::HandTrajectoryRosMessage::OVERRIDE;
    msg.unique_id = ros::Time::now().toSec();

    return msg;
}

int main(int argc, char *argv[])
{
    ros::init(argc, argv, "button_press_test");
    ros::NodeHandle nh;
    ros::Publisher hand_pub;

    tf::TransformListener tf_listener;

    std::signal(SIGINT, shutdown);

    hand_pub = nh.advertise<ihmc_msgs::HandTrajectoryRosMessage>("/ihmc_ros/valkyrie/control/hand_trajectory", 1, true);
    g_hand_pub = &hand_pub;

    ros::Rate loop(10);

    while (ros::ok())
    {
        ros::spinOnce();
//        ros::Duration(1.0).sleep();

        ihmc_msgs::HandTrajectoryRosMessage msg;
        msg = setPushButtonHandMessage(tf_listener);

        hand_pub.publish(msg);

        ROS_INFO("Published hand traj.");

//        sendRightArmTrajectory();

        break;

        loop.sleep();
    }

    return 0;
}
