/**
  * This node contains the code for performing the qual2 task.
  */

#include <ros/ros.h>
#include <csignal>
#include <boost/thread.hpp>
#include <stack>
#include <geometry_msgs/PoseStamped.h>
#include <ihmc_msgs/ArmTrajectoryRosMessage.h>
#include <ihmc_msgs/OneDoFJointTrajectoryRosMessage.h>
#include <ihmc_msgs/TrajectoryPoint1DRosMessage.h>
#include <src_walking/src_walking.hpp>
#include <src_joint_control/src_joint_control.hpp>

const double button_press[]           = { -0.75, 0.60,  0.70, 1.10, 0.0, 0.0, 0.0 };
const double slide_hand_over_button[] = {  0.42, 0.39, -0.48, 1.26, 0.0, 0.0, 0.0 };
const double arm_init_conf[]          = { -0.20, 1.20,  0.71, 1.51, 0.0, 0.0, 0.0 };

void shutdown(int signal)
{
    ros::shutdown();
}

void spinner()
{
    ros::Rate loop(20);

    while (ros::ok())
    {
        ros::spinOnce();
        loop.sleep();
    }
}

ihmc_msgs::FootstepDataListRosMessage createWalkToDoorFootsteps(src_walking::SrcWalking &val_walking)
{
    ihmc_msgs::FootstepDataListRosMessage msg;
    msg.transfer_time = 1.5;
    msg.swing_time = 1.5;
    msg.execution_mode = 0;
    msg.unique_id = -1;

    msg.footstep_data_list.push_back(val_walking.createFootStepOffset(src_walking::Side::LEFT,
                                                                      src_walking::SrcWalking::toVector3(0.4, 0.0, 0.0)));
    msg.footstep_data_list.push_back(val_walking.createFootStepOffset(src_walking::Side::RIGHT,
                                                                      src_walking::SrcWalking::toVector3(0.8, 0.0, 0.0)));
    msg.footstep_data_list.push_back(val_walking.createFootStepOffset(src_walking::Side::LEFT,
                                                                      src_walking::SrcWalking::toVector3(1.2, 0.0, 0.0)));
    msg.footstep_data_list.push_back(val_walking.createFootStepOffset(src_walking::Side::RIGHT,
                                                                      src_walking::SrcWalking::toVector3(1.6, 0.0, 0.0)));
    msg.footstep_data_list.push_back(val_walking.createFootStepOffset(src_walking::Side::LEFT,
                                                                      src_walking::SrcWalking::toVector3(2.0, 0.0, 0.0)));
    msg.footstep_data_list.push_back(val_walking.createFootStepOffset(src_walking::Side::RIGHT,
                                                                      src_walking::SrcWalking::toVector3(2.4, 0.0, 0.0)));
    msg.footstep_data_list.push_back(val_walking.createFootStepOffset(src_walking::Side::LEFT,
                                                                      src_walking::SrcWalking::toVector3(2.8, 0.0, 0.0)));
    msg.footstep_data_list.push_back(val_walking.createFootStepOffset(src_walking::Side::RIGHT,
                                                                      src_walking::SrcWalking::toVector3(2.8, 0.0, 0.0)));

    msg.footstep_data_list.push_back(val_walking.createFootStepOffset(src_walking::Side::RIGHT,
                                                                      src_walking::SrcWalking::toVector3(2.8, -0.2, 0.0)));
    msg.footstep_data_list.push_back(val_walking.createFootStepOffset(src_walking::Side::LEFT,
                                                                      src_walking::SrcWalking::toVector3(2.8, -0.2, 0.0)));

    return msg;
}

ihmc_msgs::ArmTrajectoryRosMessage createRightArmButtonPressTrajectory(src_joint_control::SrcJointControl &joint_controller)
{
    ihmc_msgs::ArmTrajectoryRosMessage msg;
    std::vector<double> button_press_vec, slide_hand_over_button_vec, arm_init_conf_vec;

    msg.robot_side = ihmc_msgs::ArmTrajectoryRosMessage::RIGHT;
    msg.unique_id = -1;

    ARRAY_TO_STD_VECTOR(button_press, button_press_vec);
    ARRAY_TO_STD_VECTOR(slide_hand_over_button, slide_hand_over_button_vec);
    ARRAY_TO_STD_VECTOR(arm_init_conf, arm_init_conf_vec);

    joint_controller.appendTrajectoryPoint(msg, 2.0, button_press_vec);
    joint_controller.appendTrajectoryPoint(msg, 3.0, slide_hand_over_button_vec);
    joint_controller.appendTrajectoryPoint(msg, 4.0, arm_init_conf_vec);

    return msg;
}

ihmc_msgs::FootstepDataListRosMessage createWalkThroughDoorFootsteps(src_walking::SrcWalking &val_walking)
{
    ihmc_msgs::FootstepDataListRosMessage msg;
    msg.transfer_time = 1.5;
    msg.swing_time = 1.5;
    msg.execution_mode = 0;
    msg.unique_id = -1;

    msg.footstep_data_list.push_back(val_walking.createFootStepOffset(src_walking::Side::LEFT,
                                                                      src_walking::SrcWalking::toVector3(0.0, 0.2, 0.0)));
    msg.footstep_data_list.push_back(val_walking.createFootStepOffset(src_walking::Side::RIGHT,
                                                                      src_walking::SrcWalking::toVector3(0.0, 0.2, 0.0)));

    msg.footstep_data_list.push_back(val_walking.createFootStepOffset(src_walking::Side::LEFT,
                                                                      src_walking::SrcWalking::toVector3(0.4, 0.2, 0.0)));
    msg.footstep_data_list.push_back(val_walking.createFootStepOffset(src_walking::Side::RIGHT,
                                                                      src_walking::SrcWalking::toVector3(0.8, 0.2, 0.0)));
    msg.footstep_data_list.push_back(val_walking.createFootStepOffset(src_walking::Side::LEFT,
                                                                      src_walking::SrcWalking::toVector3(1.2, 0.2, 0.0)));
    msg.footstep_data_list.push_back(val_walking.createFootStepOffset(src_walking::Side::RIGHT,
                                                                      src_walking::SrcWalking::toVector3(1.6, 0.2, 0.0)));
    msg.footstep_data_list.push_back(val_walking.createFootStepOffset(src_walking::Side::LEFT,
                                                                      src_walking::SrcWalking::toVector3(2.0, 0.2, 0.0)));
    msg.footstep_data_list.push_back(val_walking.createFootStepOffset(src_walking::Side::RIGHT,
                                                                      src_walking::SrcWalking::toVector3(2.0, 0.2, 0.0)));

    return msg;
}

int main(int argc, char *argv[])
{
    ros::init(argc, argv, "qual2");
    ros::NodeHandle nh;

    tf::TransformListener                   tf_listener;
    src_walking::SrcWalking                 val_walking(&nh, &tf_listener);
    src_joint_control::SrcJointControl      joint_controller(&nh, &tf_listener);
    ihmc_msgs::FootstepDataListRosMessage   footstep_msg;
    ihmc_msgs::ArmTrajectoryRosMessage      traj_msg;

    std::vector<double>                     arm_init_conf_vec;

    ARRAY_TO_STD_VECTOR(arm_init_conf, arm_init_conf_vec);

    val_walking.enableSpinner();
    joint_controller.enableSpinner();

    std::signal(SIGINT, shutdown);

    boost::thread spinner_thread(spinner);

    // Walk to the door
    std::cout << "Walking to the door." << std::endl;
    footstep_msg = createWalkToDoorFootsteps(val_walking);
    val_walking.startWalking(footstep_msg);
    std::cout << "Walking to the door finished." << std::endl;

    std::cout << std::endl;

    ros::Duration(2.0).sleep();

    // Open the door
    std::cout << "Opening the door." << std::endl;
    traj_msg = createRightArmButtonPressTrajectory(joint_controller);
    joint_controller.sendTrajectoryToRobot(traj_msg);
    ros::Duration(1.0).sleep();
    joint_controller.waitForRightArmPosition(arm_init_conf_vec);
    std::cout << "Opened the door." << std::endl;

    std::cout << std::endl;

    // Walk through the door
    std::cout << "Walking through the door." << std::endl;
    footstep_msg = createWalkThroughDoorFootsteps(val_walking);
    val_walking.startWalking(footstep_msg);
    std::cout << "Finished qual2." << std::endl;

    return 0;
}
