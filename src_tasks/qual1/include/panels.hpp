#include <perception_common/perception_common.hpp>
#include <geometry_msgs/Pose.h>

class Panel
{
    public:
        enum class Color
        {
            RED,
            GREEN,
            BLUE,
            WHITE,
            UNKNOWN
        };

        enum class Status
        {
            ON,
            OFF
        };

    private:
        geometry_msgs::Pose _position;
        Color               _color;
        Status              _status;

    public:
        Panel();
        ~Panel();

        Status getStatus();
        void setStatus(Status status);

        Color getColor();
        void setColor(Color color);

        geometry_msgs::Pose getPose();
        void setPose(geometry_msgs::Pose position);
};
