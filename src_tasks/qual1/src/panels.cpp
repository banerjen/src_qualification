#include <panels.hpp>

Panel::Panel()
{
    _status = Status::OFF;
    _color = Color::UNKNOWN;
    _position = geometry_msgs::Pose();
}

Panel::~Panel()
{}

Panel::Status Panel::getStatus()
{
    return _status;
}

void Panel::setStatus(Status status)
{
    _status = status;
}

Panel::Color Panel::getColor()
{
    return _color;
}

void Panel::setColor(Color color)
{
    _color = color;
}

geometry_msgs::Pose Panel::getPose()
{
    return _position;
}

void Panel::setPose(geometry_msgs::Pose position)
{
    _position = position;
}
