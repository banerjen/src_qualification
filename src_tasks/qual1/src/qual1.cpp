/**
  * This node contains the code for performing the qual1 task.
  */

#include <ros/ros.h>
#include <csignal>
#include <boost/thread.hpp>

#include <std_msgs/Empty.h>

#include <panels.hpp>

#include <stack>

#include <geometry_msgs/PoseStamped.h>

bool pause_;

void shutdown(int signal)
{
    ros::shutdown();
}

std::vector<cv::Rect> getBoundingBoxes(const cv::Mat &single_channel_img, cv::Mat &drawing)
{
    std::vector<std::vector<cv::Point> >    contours;
    std::vector<cv::Vec4i>                  hierarchy;
    cv::Mat                                 output_img;

    cv::threshold(single_channel_img, output_img, 200, 255, cv::THRESH_BINARY);
    cv::findContours(output_img, contours, hierarchy, cv::RETR_TREE, cv::CHAIN_APPROX_SIMPLE, cv::Point(0, 0));

    std::vector<std::vector<cv::Point> >    contours_poly(contours.size());
    std::vector<cv::Rect>                   bound_rect(contours.size());

    for (size_t i = 0; i < contours.size(); ++i)
    {
        cv::approxPolyDP(cv::Mat(contours[i]), contours_poly[i], 3, true);
        bound_rect[i] = cv::boundingRect(cv::Mat(contours_poly[i]));
    }

    for (size_t i = 0; i < contours.size(); ++i)
    {
        cv::Scalar  color = cv::Scalar(255, 255, 255);
        cv::rectangle(drawing, bound_rect[i].tl(), bound_rect[i].br(), color, 2, 8, 0);
    }

    return bound_rect;
}

Panel::Color checkColor(cv::Mat &img, cv::Rect &bounding_rect)
{
    size_t red_cnt = 0, blue_cnt = 0, green_cnt = 0, white_cnt = 0, unknown_cnt = 0;
    size_t max_cnt = 0;

    for (size_t j = 0; j < img.rows; ++j)
        for (size_t i = 0; i < img.cols; ++i)
        {
            if (bounding_rect.contains(cv::Point(i, j)) == true)
            {
                cv::Vec3b   pix = img.at<cv::Vec3b>(j, i);
                if ((pix[0] > 200) && (pix[1] > 200) && (pix[2] > 200))
                    white_cnt++;
                else if (pix[0] > 200)
                    blue_cnt++;
                else if (pix[1] > 200)
                    green_cnt++;
                else if (pix[2] > 200)
                    red_cnt++;
                else
                    unknown_cnt++;
            }
        }

    max_cnt = std::max(white_cnt, blue_cnt);
    max_cnt = std::max(green_cnt, max_cnt);
    max_cnt = std::max(red_cnt, max_cnt);
    max_cnt = std::max(unknown_cnt, max_cnt);

    if (max_cnt == white_cnt)
        return Panel::Color::WHITE;
    else if (max_cnt == blue_cnt)
        return Panel::Color::BLUE;
    else if (max_cnt == green_cnt)
        return Panel::Color::GREEN;
    else if (max_cnt == red_cnt)
        return Panel::Color::RED;
    else if (max_cnt == unknown_cnt)
        return Panel::Color::UNKNOWN;
}

geometry_msgs::Pose getPoseFromBoundingRect(cv::Mat &disp_img, cv::Mat_<double> &q_matrix, cv::Rect bounding_rect, perception_common::SrcVision &src_vision)
{
    geometry_msgs::Pose pos;
    cv::Mat img_3d;

    pos.orientation.w = 1.0;
    pos.orientation.x = 0.0;
    pos.orientation.y = 0.0;
    pos.orientation.z = 0.0;

    src_vision.get3dImage(disp_img, q_matrix, img_3d);

    std::vector<cv::Vec3f> pt_3d_vec;
    cv::Vec3f pt_3d;
    for (size_t j = 0; j < disp_img.rows; ++j)
        for (size_t i = 0; i < disp_img.cols; ++i)
        {
            if (disp_img.at<float>(cv::Point(i, j)) == 0.0)
                continue;

            if (bounding_rect.contains(cv::Point(i, j)) == true)
            {
                pt_3d = src_vision.get3dPoint(img_3d, j, i);
                if (std::sqrt(std::pow(pt_3d[0], 2) + std::pow(pt_3d[1], 2) + std::pow(pt_3d[2], 2)) < 10)
                    pt_3d_vec.push_back(pt_3d);
            }
        }

    double x, y, z;
    for (size_t i = 0; i < pt_3d_vec.size(); ++i)
    {
//        std::cout << "Position: " << pt_3d_vec[i].val[0] << ", " <<
//                                     pt_3d_vec[i].val[1] << ", " <<
//                                     pt_3d_vec[i].val[2] << std::endl;
        x += pt_3d_vec[i].val[0];
        y += pt_3d_vec[i].val[1];
        z += pt_3d_vec[i].val[2];
    }

    pos.position.x = x / pt_3d_vec.size();
    pos.position.y = y / pt_3d_vec.size();
    pos.position.z = z / pt_3d_vec.size();

    std::cout << "Position: " << pos.position.x << ", " <<
                                 pos.position.y << ", " <<
                                 pos.position.z << std::endl;

    return pos;
}

cv::Mat compareTwoImages(const cv::Mat &img1, const cv::Mat &img2, cv::Mat &img3)
{
    if (!((img1.rows > 0) && (img1.cols > 0) && (img2.rows > 0) && (img2.cols > 0)))
        return cv::Mat();

    cv::Mat diff_img, foreground_mask;

    cv::absdiff(img1, img2, diff_img);
    foreground_mask = cv::Mat::zeros(diff_img.rows, diff_img.cols, CV_8UC1);

    size_t num_pixels = 0;

    int erosion_size = 1;
    cv::Mat element = cv::getStructuringElement(cv::MORPH_RECT,
                                                cv::Size(2*erosion_size + 1, 2*erosion_size + 1),
                                                cv::Point(erosion_size, erosion_size));
    cv::erode(diff_img, diff_img, element);

    img3 = diff_img.clone();

    for (size_t j = 0; j < diff_img.rows; ++j)
        for (size_t i = 0; i < diff_img.cols; ++i)
        {
            cv::Vec3b pix = diff_img.at<cv::Vec3b>(j, i);

            if ((pix[0] > 200) || (pix[1] > 200) || (pix[2] > 200))
            {
                pause_ = true;
                num_pixels++;
                foreground_mask.at<unsigned char>(j, i) = 255;
            }
        }

//    if (num_pixels > 0)
//        std::cout << "Num pixels: " << num_pixels << std::endl;

    return foreground_mask.clone();
}

double timeDiffHeader(const std_msgs::Header &header_1, const std_msgs::Header &header_2)
{
    ros::Time   time_1 = header_1.stamp;
    ros::Time   time_2 = header_2.stamp;

    return (std::abs(time_1.toSec() - time_2.toSec()));
}

int main(int argc, char *argv[])
{
    ros::init(argc, argv, "qual1");
    ros::NodeHandle nh;

    std::signal(SIGINT, shutdown);

    perception_common::SrcVision    src_vision(&nh);

    cv::namedWindow("qual1_img1", cv::WINDOW_AUTOSIZE);

    std_msgs::Empty start_msg;
    ros::Publisher  start_task_pub;
    ros::Publisher  pose_pub;

    start_task_pub = nh.advertise<std_msgs::Empty>("/srcsim/qual1/start", 1, true);    
    pose_pub = nh.advertise<geometry_msgs::PoseStamped>("/panel_pose", 1, true);

    ros::Rate loop(10);

    cv::Mat img, display_img, display_img2;
    cv::Mat img1, img2, img3;
    cv::Mat disp_img, img_3d;
    cv::Mat_<double> q_matrix;
    bool first = false;

    pause_ = false;

    std::vector<cv::Rect>   bounding_rect;
    std_msgs::Header        img1_header, img2_header;
    Panel::Color            last_color;

    Panel                   cur_panel;
    std::vector<Panel>      all_panels;
    std::stack<Panel>       stack_panels;

    bool                    finished = false;
    size_t                  frame_count = 0;

    q_matrix = src_vision.getQMatrix();

    cur_panel.setStatus(Panel::Status::OFF);
    cur_panel.setColor(Panel::Color::UNKNOWN);

    geometry_msgs::PoseStamped  pose_stamped;

    pose_stamped.header.frame_id = "left_camera_optical_frame";

    while (ros::ok() && (finished == false))
    {
        if (frame_count > 5)
            start_task_pub.publish(start_msg);

        ros::spinOnce();
        if (first == false)
        {
            while (src_vision.getLeftCameraImage(img1, img1_header) == false)
                ros::spinOnce();
            display_img = img1.clone();
            src_vision.getDisparityImage(disp_img);
            if ((img1.cols > 0) && (img1.rows > 0))
            {
                cv::GaussianBlur(img1, img1, cv::Size(5, 5), 0, 0);
                frame_count++;
            }
        }
        else if (first == true)
        {
            while (src_vision.getLeftCameraImage(img2, img2_header) == false)
                ros::spinOnce();
            display_img = img2.clone();
            src_vision.getDisparityImage(disp_img);
            if ((img2.cols > 0) && (img2.rows > 0))
            {
                cv::GaussianBlur(img2, img2, cv::Size(5, 5), 0, 0);
                frame_count++;
            }
        }

        if (first == false)
            first = true;
        else if (first == true)
            first = false;

        img = compareTwoImages(img1, img2, img3);

        if ((img.rows > 0) && (img.cols > 0))
        {
            if (pause_ == true)
            {
                display_img2 = display_img.clone();

                bounding_rect = getBoundingBoxes(img, display_img2);

                for (size_t i = 0; i < bounding_rect.size(); ++i)
                {
                    Panel::Color color;
                    cv::Rect rect = bounding_rect[i];
                    color = checkColor(display_img, rect);

                    if (color == Panel::Color::RED)
                    {
                        cur_panel.setColor(color);
                        cur_panel.setStatus(Panel::Status::ON);
                        cur_panel.setPose(getPoseFromBoundingRect(disp_img, q_matrix, rect, src_vision));
                        pose_stamped.pose = cur_panel.getPose();
                        pose_pub.publish(pose_stamped);
                        stack_panels.push(cur_panel);
                        all_panels.push_back(cur_panel);
                        std::cout << "Panel color : RED" << std::endl;
                        cv::Mat tmp_img;
                        std_msgs::Header tmp_header;
                        for (size_t tmp_i = 0; tmp_i < 5; ++tmp_i)
                            while (src_vision.getLeftCameraImage(tmp_img, tmp_header) == false)
                                ros::spinOnce();
                    }
                    else if (color == Panel::Color::BLUE)
                    {
                        cur_panel.setColor(color);
                        cur_panel.setStatus(Panel::Status::ON);
                        cur_panel.setPose(getPoseFromBoundingRect(disp_img, q_matrix, rect, src_vision));
                        pose_stamped.pose = cur_panel.getPose();
                        pose_pub.publish(pose_stamped);
                        stack_panels.push(cur_panel);
                        all_panels.push_back(cur_panel);
                        std::cout << "Panel color : BLUE" << std::endl;
                        cv::Mat tmp_img;
                        std_msgs::Header tmp_header;
                        for (size_t tmp_i = 0; tmp_i < 5; ++tmp_i)
                            while (src_vision.getLeftCameraImage(tmp_img, tmp_header) == false)
                                ros::spinOnce();
                    }
                    else if (color == Panel::Color::GREEN)
                    {
                        cur_panel.setColor(color);
                        cur_panel.setStatus(Panel::Status::ON);
                        cur_panel.setPose(getPoseFromBoundingRect(disp_img, q_matrix, rect, src_vision));
                        pose_stamped.pose = cur_panel.getPose();
                        pose_pub.publish(pose_stamped);
                        stack_panels.push(cur_panel);
                        all_panels.push_back(cur_panel);
                        std::cout << "Panel color : GREEN" << std::endl;
                        cv::Mat tmp_img;
                        std_msgs::Header tmp_header;
                        for (size_t tmp_i = 0; tmp_i < 5; ++tmp_i)
                            while (src_vision.getLeftCameraImage(tmp_img, tmp_header) == false)
                                ros::spinOnce();
                    }
                    else if (color == Panel::Color::WHITE)
                    {
                        cur_panel.setColor(color);
                        cur_panel.setStatus(Panel::Status::ON);
                        cur_panel.setPose(getPoseFromBoundingRect(disp_img, q_matrix, rect, src_vision));
                        pose_stamped.pose = cur_panel.getPose();
                        pose_pub.publish(pose_stamped);
                        stack_panels.push(cur_panel);
                        all_panels.push_back(cur_panel);
                        std::cout << "Panel color : WHITE" << std::endl;
                        cv::Mat tmp_img;
                        std_msgs::Header tmp_header;
                        for (size_t tmp_i = 0; tmp_i < 5; ++tmp_i)
                            while (src_vision.getLeftCameraImage(tmp_img, tmp_header) == false)
                                ros::spinOnce();
                    }
                    else if (color == Panel::Color::UNKNOWN)
                    {
                        double dist = std::numeric_limits<double>::max();
                        std::cout << "OFF." << std::endl;
                        if (stack_panels.empty() == false)
                        {
                            cur_panel = stack_panels.top();
                            cur_panel.setStatus(Panel::Status::OFF);
                            geometry_msgs::Pose p;
                            p = getPoseFromBoundingRect(disp_img, q_matrix, rect, src_vision);
                            dist = std::sqrt(std::pow((p.position.x - cur_panel.getPose().position.x), 2) +
                                             std::pow((p.position.y - cur_panel.getPose().position.y), 2) +
                                             std::pow((p.position.z - cur_panel.getPose().position.z), 2));
                            if (dist < 0.05)
                            {
                                pose_stamped.pose = cur_panel.getPose();
                                pose_pub.publish(pose_stamped);
                                all_panels.push_back(cur_panel);
                                stack_panels.pop();
                            }                            
                        }
                        else
                            std::cout << "Tried to pop a Panel when the stack was empty." << std::endl;
                        if (cur_panel.getColor() == Panel::Color::WHITE)
                            if (dist < 0.05)
                                finished = true;
                        std::cout << "Stack size : " << stack_panels.size() << std::endl;
                        cv::Mat tmp_img;
                        std_msgs::Header tmp_header;
                        for (size_t tmp_i = 0; tmp_i < 5; ++tmp_i)
                            while (src_vision.getLeftCameraImage(tmp_img, tmp_header) == false)
                                ros::spinOnce();
                    }

                    last_color = color;
                }

                pause_ = false;
            }
        }

        if ((display_img2.rows > 0) && (display_img2.cols > 0))
        {
            cv::Mat display_img3;
            cv::flip(display_img2, display_img3, -1);
            cv::imshow("qual1_img1", display_img3);
            ros::spinOnce();
            cv::waitKey(1);
        }
        else
            ros::spinOnce();

        loop.sleep();
    }

    std::cout << "Finished detecting all the panel lights." << std::endl;
    for (size_t i = 0; i < all_panels.size(); ++i)
    {
        Panel panel = all_panels[i];
        std::string color, status;

        if (panel.getColor() == Panel::Color::BLUE)
            color = "BLUE";
        else if (panel.getColor() == Panel::Color::GREEN)
            color = "GREEN";
        else if (panel.getColor() == Panel::Color::RED)
            color = "RED";
        else if (panel.getColor() == Panel::Color::WHITE)
            color = "WHITE";
        else if (panel.getColor() == Panel::Color::UNKNOWN)
            color = "UNKNOWN";
        else
            color = "SOMETHING IS WRONG!";

        if (panel.getStatus() == Panel::Status::ON)
            status = "ON";
        else if (panel.getStatus() == Panel::Status::OFF)
            status = "OFF";
        else
            status = "SOMETHING IS WRONG";

        std::cout << "Panel [" << i << "] : " << color << "\t - " << status << "\t" << std::endl;
        std::cout << panel.getPose().position.x << ", " << panel.getPose().position.y << ", " <<
                     panel.getPose().position.z << std::endl;
        if (status == "ON")
            std::cout << "Dist : " << std::sqrt(panel.getPose().position.x * panel.getPose().position.x +
                                                panel.getPose().position.y * panel.getPose().position.y +
                                                panel.getPose().position.z * panel.getPose().position.z) << std::endl;

        if (status == "OFF")
            std::cout << std::endl;
    }

    return 0;
}

//cv::Mat bgr[3];
//cv::split(img3, bgr);
//    cv::Mat drawing = cv::Mat::zeros(output_img.size(), CV_8UC3);
