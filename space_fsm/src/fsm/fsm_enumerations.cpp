#include "space_fsm/fsm_enumerations.h"
#include "iostream"

using namespace SPACE_FSM;

std::map<States, std::string> state_names;
std::map<Events, std::string> event_names;
std::map<Actions, std::string> action_names;

const std::string error_state_name      = "State name not found.";
const std::string error_event_name      = "Event name not found.";
const std::string error_action_name     = "Action name not found.";

void initStrings()
{
    state_names[States::IDLE]               = "IDLE";
    state_names[States::RESET_ROBOT]        = "RESET_ROBOT";
    state_names[States::END]                = "END";

    event_names[Events::Start]              = "Start";
    event_names[Events::ResetRobot]         = "ResetRobot";
    event_names[Events::FailedAction]       = "FailedAction";
    event_names[Events::NoEvent]            = "NoEvent";
    event_names[Events::End]                = "End";

    action_names[Actions::NoAction]         = "NoAction";
    action_names[Actions::Initialize]       = "Initialize";
    action_names[Actions::Repeat]           = "Repeat";
    action_names[Actions::ResetRobot]       = "ResetRobot";
    action_names[Actions::Exit]             = "Exit";
}

const std::string getStateName(States state)
{
    if (state_names.find(state) != state_names.end())
        return state_names[state];
    else
        return error_state_name;
}

const std::string getEventName(Events event)
{
    if (event_names.find(event) != event_names.end())
        return event_names[event];
    else
        return error_event_name;
}

const std::string getActionName(Actions action)
{
    if (action_names.find(action) != action_names.end())
        return action_name[action];
    else
        return error_action_name;
}
