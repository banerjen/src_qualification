#pragma once

#include "ros/ros.h"
#include "boost/thread.hpp"
#include "boost/thread/mutex.hpp"

// Shared variables
extern ros::NodeHandle  *g_nh, *g_pnh;
extern bool             is_paused;
extern bool             stop_action;
extern boost::mutex     mt_pause;
extern boost::mutex     mt_stop;
extern bool             g_debug_mode;
extern bool             g_last_return_value;

#define ACTION_BLOCK_BREAKPOINT()

#define BEGIN_ACTION_BLOCK \
    ACTION_BLOCK_BREAKPOINT()

#define END_ACTION_BLOCK \
    ACTION_BLOCK_BREAKPOINT() \
    boost::mutex::scoped_lock lock(stop_mutex); \
    stop_action = false;

#define ACTION_FUNCTION(arg) \
    action_thread
