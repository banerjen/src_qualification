#pragma once

#include "space_fsm/fsm_enumerations.h"
#include "space_fsm/fsm.h"

class SrcStateMachine: public StateMachine<SPACE_FSM::States, SPACE_FSM::Events, SPACE_FSM::Actions>
{
    public:
        std::map<SPACE_FSM::Events, int>    event_priorities;
        bool                                initialized;

        SrcStateMachine(std::vector<fsm_tuple_> rows);
        ~SrcStateMachine();
        void initStateMachine();
        void pause();
        void resume();
        void stop();

    protected:
        SPACE_FSM::Events doAction(SPACE_FSM::Actions action);
        void state_machine(SPACE_FSM::States &state, SPACE_FSM::Events event);
        SPACE_FSM::Events resolveEventConflict(SPACE_FSM::Events e1, SPACE_FSM::Events e2);
        SPACE_FSM::Events repeat();
        void initPriorities();
        void initRepeatTable();

        SPACE_FSM::Actions                                  m_last_action;
        std::map<SPACE_FSM::Actions, SPACE_FSM::Events>     m_repeat_action;
        boost::thread                                       *m_action_thread;
};

extern SrcStateMachine      *src_state_machine;
extern bool                 sigint_received;
extern SPACE_FSM::Events    g_last_event;
extern SPACE_FSM::Events    g_interrupt_event;
extern SPACE_FSM::States    g_current_state;
extern SPACE_FSM::Actions   g_last_action;
extern bool                 is_paused;
extern boost::mutex         mt_pause;

SrcStateMachine::fsm_tuple_ createTuple(SPACE_FSM::States   state,
                                        SPACE_FSM::States   next_state,
                                        SPACE_FSM::Events   event,
                                        SPACE_FSM::Actions  action);

void initStateTransitionTable();
