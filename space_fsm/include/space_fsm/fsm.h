#pragma once

#include "iostream"
#include "cstddef"
#include "vector"

template <typename TS, typename TE, typename TA>
class StateMachine
{
    public:
        typedef struct
        {
            TS state;
            TE event;
            TA action;
            TS next_state;
        } fsm_tuple_;

        StateMachine()
        {}

        virtual ~StateMachine()
        {}

        void StartFSM(TS state, TE event)
        {
            state_machine(state, event);
        }

    protected:
        fsm_tuple_  *transition_table;
        size_t      size_transition_table;

        void StateError(TA action)
        {
            std::cout << "Error: Overloaded action not found." << std::endl;
            exit(-1);
        }

        void StateError(TS state, TE event)
        {
            std::cout << "Missing state transition table row." << std::endl;
        }

        const fsm_tuple_ *findTuple(TS state, TE event)
        {
            size_t i = 0;

            while (i < size_transition_table)
            {
                fsm_tuple_ row = transition_table[i];

                if ((row.state == state) && (row.event == event))
                    return &transition_table[i];

                ++i;
            }

            StateError(state, event);
            return NULL;
        }

        virtual TE doAction(TA action) = 0;

        virtual void state_machine(TS &state, TE event) = 0;
};
