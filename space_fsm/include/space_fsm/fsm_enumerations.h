#pragma once

#include "string"
#include "vector"
#include "map"

namespace SPACE_FSM
{
    enum class States
    {
        IDLE,
        RESET_ROBOT,
        END
    };

    enum class Events
    {
        Start,
        ResetRobot,
        FailedAction,
        NoEvent,
        End
    };

    enum class Actions
    {
        NoAction,
        Initialize,
        Repeat,
        ResetRobot,
        Exit
    };

    extern std::map<States, std::string>    state_names;
    extern std::map<Events, std::string>    event_names;
    extern std::map<Actions, std::string>   action_names;

    void initStrings();
}

const std::string getStateName(SPACE_FSM::States state);
const std::string getEventName(SPACE_FSM::Events event);
const std::string getActionName(SPACE_FSM::Actions action);
